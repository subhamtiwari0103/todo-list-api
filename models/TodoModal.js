const mongoose = require('mongoose')

const Schema = mongoose.Schema

const todoModelSchema = new Schema({
  title:{
    type:String,
    required:true
  },
  text:{
    type:String,
    required:true
  },
  created_at:{
    type:Date,
    required:true
  },
  updated_at:{
    type:Date,
  },
})

module.exports = mongoose.model("Todo", todoModelSchema)