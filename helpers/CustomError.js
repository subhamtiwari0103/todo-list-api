const { validationResult } = require('express-validator');


class CustomError extends Error{
  constructor(errors, ...params){
    super(...params)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, CustomError)
    }
    this.errors = errors
  }

  static handleError(req){
    const errors = validationResult(req)
    let error= null
    if (!errors.isEmpty()) {
      error = new CustomError(errors)
      error.statusCode = 422
      error.data = errors.array()
      return {error, isError:true}
    }else{
      return {error, isError:false}
    }
  }
  
  static throwError(err, next){
    if(!err.statusCode){
      err.statusCode = 500
    }
    next(err)
  }
}


module.exports = CustomError