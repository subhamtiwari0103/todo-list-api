require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors')
const todoRouter = require('./routers/todoRouter')
const PORT = process.env.PORT

const db_user = process.env.DB_USER
const db_pass = process.env.DB_PASSWORD


const app = express();
app.use(bodyParser.json());
app.use(cors())

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

app.use('/api/todo/',todoRouter)

app.use((error, req, res, next) => {
  console.log(error);
  const status = error.statusCode || 500;
  const message = error.message;
  const data = error.data;
  res.status(status).json({ message: message, data: data });
});

mongoose.connect(
   `mongodb+srv://${db_user}:${db_pass}@cluster0.yhfae.mongodb.net/todo_list?retryWrites=true&w=majority`, 
   {useNewUrlParser: true,useCreateIndex:true, useUnifiedTopology: true}
 ).then(() => {
    console.log('port',PORT)
    app.listen(PORT);
 }).catch(err=> {
   console.log("db err", err)
})