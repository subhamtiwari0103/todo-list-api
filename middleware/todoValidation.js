const { body } = require('express-validator');

exports.todoPostData = [
  body('title').not().isEmpty().trim().escape(),
  body('text').not().isEmpty().trim().escape(),
]

exports.todoPatchData = [
  body('title').if(body('title').exists()).trim().escape(),
  body('text').if(body('text').exists()).trim().escape(),
]
