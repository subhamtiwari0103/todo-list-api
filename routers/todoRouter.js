const express = require('express');
const TodoController = require('../controllers/TodoController');
const todoValidation  = require('../middleware/todoValidation')

const router = express.Router();

router.get('/',  TodoController.getAllTodoList)

router.post('/', todoValidation.todoPostData, TodoController.createTodo)

router.patch('/:id', todoValidation.todoPatchData, TodoController.editTodo)

router.delete('/:id', TodoController.deleteTodo)


module.exports = router