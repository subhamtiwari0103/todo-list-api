const Todo = require('../models/TodoModal')
const moment = require('moment-timezone');
const CustomError = require('../helpers/CustomError')
const utc = 1502212611;

class TodoController{
  async getAllTodoList(req, res, next){
    try{
      let todos = await Todo.find()
      res.status(200).json(todos)
    }catch(err){
      CustomError.throwError(err, next)
    }
  }
  async createTodo(req, res, next){
    const validate = CustomError.handleError(req)
    if(validate.isError){
      return next(validate.error)
    }
    try{
      let data = {...req.body}
      data.created_at = moment(new Date()).utc(utc).tz('Asia/Kolkata').format('YYYY-MM-DD HH:mm:ss')
      let todo = await new Todo(data).save()
      res.status(201).json(todo)
    }catch(err){
      CustomError.throwError(err, next)
    }
  }
  
  async editTodo(req, res, next){
    const validate = CustomError.handleError(req)
    if(validate.isError){
      return next(validate.error)
    }
    try{
      let data = {...req.body}
      let _id = req.params.id
      data.updated_at =  moment(new Date()).utc(utc).tz('Asia/Kolkata').format('YYYY-MM-DD HH:mm:ss');
      let todo = await Todo.findByIdAndUpdate(_id, data, {new:true})
      res.status(200).json(todo)
    }catch(err){
      CustomError.throwError(err,next)
    }
  }
  async deleteTodo(req, res, next){
    let _id = req.params.id
    try{
      await Todo.findByIdAndDelete({_id})
      res.status(204).json()
    }catch(err){
      CustomError.throwError(err,next)
    }
  }
}

module.exports = new TodoController()